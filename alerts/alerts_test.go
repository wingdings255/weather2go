package alerts_test

import (
	"io/ioutil"
	"testing"

	"weather2go/alerts"
)

// Test with json file withour alert object
func TestAlertWOalert(t *testing.T) {
	file, err := ioutil.ReadFile("../testdata/EXAMPLE-weather.json")
	if err != nil {
		t.Fatalf("Failed to read json")
	}
	data, err := alerts.Parse(file)
	if err != nil {
		t.Fatalf("Failed to parse json")
	}
	if alerts.Check(data) {
		t.Fatalf("Falsly detected alert data")
	}
}

// Test json file with alert
func TestAlertWalert(t *testing.T) {
	file, err := ioutil.ReadFile("../testdata/EXAMPLE-weather-wAlert.json")
	if err != nil {
		t.Fatalf("Failed to read json")
	}
	data, err := alerts.Parse(file)
	if err != nil {
		t.Fatalf("Failed to parse json")
	}
	if !alerts.Check(data) {
		t.Fatalf("Didn't detected alert data")
	}
}
