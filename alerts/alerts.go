package alerts

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"text/tabwriter"
	"time"
)

type alertData struct {
	Alerts []Alerts `json:"alerts,omitempty"`
}

type Alerts struct {
	Sender_name string   `json:"sender_name"`
	Event       string   `json:"event"`
	Start       float64  `json:"start"`
	End         float64  `json:"end"`
	Description string   `json:"description"`
	Tags        []string `json:"tags"`
}

func Check(alert alertData) bool {

	if len(alert.Alerts) != 0 {
		return true
	} else {
		return false
	}
}

func Parse(data []byte) (alertData, error) {

	var alerts alertData
	err := json.Unmarshal(data, &alerts)
	if err != nil {
		return alerts, errors.New("cant parse JSON data")
	} else {
		return alerts, nil
	}
}

func Display(alert alertData) {
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)
	defer w.Flush()
	for i := 0; i < len(alert.Alerts); i++ {
		s := time.Unix(int64(alert.Alerts[i].Start), 0)
		e := time.Unix(int64(alert.Alerts[i].End), 0)
		fmt.Println("-----!!ALERT!!-----")
		fmt.Fprintf(w, "FROM: \t %s\n", alert.Alerts[i].Sender_name)
		fmt.Fprintf(w, "TAGS \t %s\n", alert.Alerts[i].Tags)
		fmt.Fprintf(w, "TITLE: \t %s\n", alert.Alerts[i].Event)
		fmt.Fprintf(w, "TIME: \t %s to %s\n", s.Format(time.Kitchen), e.Format(time.Kitchen))
		fmt.Fprintf(w, "MESSAGE: \n%s\n", alert.Alerts[i].Description)
	}
}
