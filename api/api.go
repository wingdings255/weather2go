package api

import (
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
)

func Fetch(endpoint string, args []string, apiKey string) ([]byte, error) {

	params := strings.Join(args, "")
	url := []string{endpoint, params, "&appid=", apiKey}

	//api := strings.Join(url, "")
	//fmt.Printf("Calling API at %s \n", api)

	resp, err := http.Get(strings.Join(url, ""))
	if err != nil {
		var data []byte
		return data, errors.New("http error")
	}
	//fmt.Printf("HTTP status code: %s \n", resp.Status)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return body, errors.New("cant read response body")
	}
	defer resp.Body.Close()

	return body, nil
}
