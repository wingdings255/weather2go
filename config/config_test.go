package config_test

import (
	"io/ioutil"
	"testing"

	"weather2go/config"
)

func TestConfigureKEY(t *testing.T) {
	file, err := ioutil.ReadFile("../testdata/EXAMPLE-config.json")
	if err != nil {
		t.Fatalf("Failed to read json file")
	}
	config, err := config.Configure([]byte(file))
	if config.Key == "" || err != nil {
		t.Fatalf("Failed to assign .key property to config object")
	}
}

func TestConfigureCords(t *testing.T) {
	file, err := ioutil.ReadFile("../testdata/EXAMPLE-config.json")
	if err != nil {
		t.Fatalf("Failed to read json")
	}
	config, err := config.Configure([]byte(file))
	if config.Lat == "" || config.Lon == "" || err != nil {
		t.Fatalf("Failed to load .lat or .lon properties to config object")
	}
}
