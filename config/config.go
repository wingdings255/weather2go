package config

import (
	"encoding/json"
	"errors"
)

type Configuration struct {
	Key  string `json:"key"`
	Lat  string `json:"lat"`
	Lon  string `json:"lon"`
	Unit string `json:"unit"`
}

func Configure(cfg []byte) (Configuration, error) {

	if !json.Valid(cfg) {
		var config Configuration
		return config, errors.New("JSON data is not valid")
	} else {
		var config Configuration
		err := json.Unmarshal(cfg, &config)
		if err != nil {
			return config, errors.New("Cant marshal JSON")
		}
		if len(config.Key) != 32 {
			return config, errors.New("Invalid key")
		}
		return config, nil
	}
}
