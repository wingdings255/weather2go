package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"weather2go/alerts"
	"weather2go/api"
	"weather2go/config"
	"weather2go/current"
)

const onecall string = "https://api.openweathermap.org/data/2.5/onecall"

//const aqi string = "http://api.openweathermap.org/data/2.5/air_pollution"

func main() {
	log.SetPrefix("weather2go: ")
	log.SetFlags(0)

	cfgF, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Fatal(err)
	}
	config, err := config.Configure(cfgF)
	if err != nil {
		log.Fatal(err)
	}

	arglen := len(os.Args[1:])
	if arglen == 0 {
		arg := "help"
		help(arg)
	} else {
		arg := string(os.Args[1])
		switch arg {
		case "help":
			help(arg)
		case "current":
			apiArgs := []string{"?lat=", config.Lat, "&lon=", config.Lon, "&units=", config.Unit, "&exclude=minutely"}
			data, err := api.Fetch(onecall, apiArgs, config.Key)
			if err != nil {
				log.Fatal(err)
			}
			if !json.Valid(data) {
				log.Fatal("Json data is not valid")
			}
			curData, err := current.Parse(data)
			if err != nil {
				log.Fatal(err)
			}
			current.Display(curData)

			alert, err := alerts.Parse([]byte(data))
			if err != nil {
				log.Fatal(err)
			}
			if alerts.Check(alert) {
				alerts.Display(alert)
			} else {
				os.Exit(0)
			}

		default:
			fmt.Println("Option:", arg, "not found. Refer to help command")
			cmd := "help"
			help(cmd)
		}
	}
}

func help(cmd string) {

	switch cmd {
	case "help":
		fmt.Println("Options are [ current, help ]")
		os.Exit(1)
	}
}
