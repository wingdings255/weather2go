package current

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"text/tabwriter"
	"time"
)

type currentData struct {
	Current struct {
		DT         int64     `json:"dt"`
		Sunrise    int64     `json:"sunrise"`
		Sunset     int64     `json:"sunset"`
		Temp       float64   `json:"temp"`
		Feels_like float64   `json:"feels_like"`
		Pressure   float64   `json:"pressure"`
		Humidity   float64   `json:"humidity"`
		Dew_point  float64   `json:"dew_point"`
		UVI        float64   `json:"uvi"`
		Clouds     float64   `josn:"clouds"`
		Visibility float64   `json:"visibility"`
		Wind_speed float64   `json:"wind_speed"`
		Wind_deg   float64   `json:"wind_deg"`
		Wind_gust  float64   `json:"wind_gust"`
		Weather    []weather `json:"weather"`
	} `json:"current"`
}

type weather struct {
	ID          int64  `json:"id"`
	Main        string `json:"main"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
}

func Parse(data []byte) (currentData, error) {
	var currData currentData
	err := json.Unmarshal(data, &currData)
	if err != nil {
		return currData, errors.New("cant parse JSON data")
	} else {
		return currData, nil
	}
}

func Display(data currentData) {
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)
	defer w.Flush()
	t := time.Unix(data.Current.DT, 0)
	fmt.Printf("Current weather for %s \n", t.Format(time.Kitchen))
	fmt.Println("--------------------------")
	fmt.Printf("Description: %s \n", data.Current.Weather[0].Description)
	fmt.Fprintf(w, "Temperature: %g F\tFeels Like: %g \n", data.Current.Temp, data.Current.Feels_like)
	fmt.Fprintf(w, "Cloud Cover: %g%%\tUV Level: %g \n", data.Current.Clouds, data.Current.UVI)
	fmt.Fprintf(w, "Avg Wind Speed: %g mph \tWind Temp: %g\n", data.Current.Wind_speed, data.Current.Wind_deg)
	fmt.Fprintf(w, "Humidity: %g%%\tPressure: %g hPa\n", data.Current.Humidity, data.Current.Pressure)
	sS := time.Unix(data.Current.Sunset, 0)
	sR := time.Unix(data.Current.Sunrise, 0)
	fmt.Fprintf(w, "Sunrise: %s\tSunset: %s \n", sR.Format(time.Kitchen), sS.Format(time.Kitchen))

}
